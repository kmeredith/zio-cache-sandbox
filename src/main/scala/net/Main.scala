package net

import sttp.capabilities.{Effect, WebSockets}
import sttp.capabilities.zio.ZioStreams
import sttp.client3.{Request, Response, SttpBackend}
import sttp.model.StatusCode
import sttp.monad.MonadError

import java.time.{Duration, Instant}
import zio._
import zio.stream.{ZSink, ZStream}
import zio.clock._

final class InMemoryHealthCheck(result: Task[Boolean]) extends HealthCheck {
  override def isHealthy: Task[Boolean] = result
}
object InMemoryHealthCheck {
  def withMultipleResponses(responses: List[Task[Boolean]]): TaskLayer[Has[HealthCheck]] = {
    def mkClient(ref: Ref[List[Task[Boolean]]]): HealthCheck = {
      val whenExhausted: Task[Boolean] =
        Task.fail(new RuntimeException("exhausted!"))

      new InMemoryHealthCheck(
        ref
          .modifySome(whenExhausted) { case head :: tail =>
            (head, tail)
          }
          .flatten
      )
    }
    ZLayer.fromEffect(Ref.make(responses).map { mkClient(_) })
  }
}

object Main extends App {

  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    for {
      appIsHealthy <-
        Aggregate
          .isAppHealthy
          .provideSomeLayer[Clock](
            InMemoryHealthCheck
              .withMultipleResponses(List(Task.succeed(true), Task.succeed(true)))
          )
      _ <- console.putStrLn(">> app is healthy:" + appIsHealthy)
    } yield ExitCode.success
  }.orDie
}
