package net

import zio._
import zio.clock.Clock

import java.time.Duration

object Aggregate {

  def isAppHealthy: RIO[Has[HealthCheck] with Clock, Boolean] =
    for {
      hc <- ZIO.service[HealthCheck]
      res1 <- hc.isHealthy
      _ <- ZIO.sleep(Duration.ofMillis(100))
      res2 <- hc.isHealthy
    } yield res1 && res2

}
