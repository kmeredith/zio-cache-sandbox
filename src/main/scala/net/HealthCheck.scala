package net

import sttp.capabilities.WebSockets
import sttp.capabilities.zio.ZioStreams
import zio.{Ref, Task, ZIO}
import sttp.client3.quick._
import sttp.client3.{Response, ResponseAsByteArray, ResponseException, SttpBackend, basicRequest}
import sttp.model.{StatusCode, Uri}

trait HealthCheck {
  def isHealthy: Task[Boolean]
}
object LiveHealthCheck {
  type AsyncZIOClient = SttpBackend[Task, ZioStreams with WebSockets]
}
final class LiveHealthCheck(base: Uri, sttpBackend: LiveHealthCheck.AsyncZIOClient) extends HealthCheck {

  override def isHealthy: Task[Boolean] = {

    val uri = base.copy(
      pathSegments = base.pathSegments.add(List("health"))
    )

    basicRequest
      .post(uri)
      .response(asString)
      .send(sttpBackend)
      .map(_.code)
      .map {
        case StatusCode.Ok => true
        case _ => false
    }
  }
}
