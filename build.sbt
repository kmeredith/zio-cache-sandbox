scalaVersion := "2.13.6"

val SttpVersion = "3.5.0"

libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % "1.0.13",
  "com.softwaremill.sttp.client3" %% "async-http-client-backend-zio1" % SttpVersion,
  "com.softwaremill.sttp.client3" %% "zio1-json" % SttpVersion
)

fork := true